import React from 'react'
import { DrawerNavigator } from 'react-navigation'
import MyNewScreen from '../Containers/MyNewScreen'
import ListviewExample from '../Containers/ListviewExample'
import CardExample from '../Containers/CardExample'
import DrawerContent from '../Containers/DrawerContent'

const NavigationDrawer = DrawerNavigator({
  MyNewScreen: {screen: MyNewScreen},
  ListviewExample: {screen: ListviewExample},
  CardExample: {screen: CardExample}
},
  {
    initialRouteName: 'ListviewExample',
    contentComponent: props => <DrawerContent {...props} />
  }
)

export default NavigationDrawer
